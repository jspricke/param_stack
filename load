#!/usr/bin/python
# Software License Agreement (BSD License)
#
# Copyright (c) 2022, Jochen Sprickerhof
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


from os import environ, path
from sys import argv

import rosparam
from catkin_pkg.workspaces import get_spaces
from rosmaster.paramserver import ParamDictionary
from rospkg import RosPack
from rospkg.environment import get_ros_home
from yaml import safe_dump


class MasterMock:
    p = ParamDictionary(None)

    def setParam(self, key, value):
        return self.p.set_param(key, value)

    def getParam(self, key):
        return self.p.get_param(key)


def main():
    master = MasterMock()
    rosparam.get_param_server = lambda: master
    package = path.dirname(RosPack().get_path(argv[1]))
    config = [path.join(path.dirname(get_spaces()[0]), "config")]

    conffile = path.join(get_ros_home(), "param_stack.conf")
    if path.isfile(conffile):
        with open(conffile) as cfile:
            config = cfile.read().splitlines()

    var = "ROS_PARAM_STACK_DIRS"
    if var in environ:
        config = environ[var].split(":")

    for pth in [package] + config:
        yml = path.join(pth, argv[1], argv[2])
        if path.isfile(yml):
            rosparam.yamlmain([None, "load", yml])
    print(safe_dump(rosparam.get_param("/")))


if __name__ == "__main__":
    main()
