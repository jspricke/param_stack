# param_stack

Script for roslaunch to load YAML files with parameters from multiple locations.
See the [launchfile](test/test.launch) for an example.
By default it loads files from the package and afterwards from `catkin_ws/config`.
You can specify other paths either in `~/.ros/param_stack.conf` or `$ROS_PARAM_STACK_DIRS`.

## Use case

Multiple robots sharing the same packages but need tweaks to some parameters.
Have the default parameters in the/a package and add the diff to `catkin_ws/config`.
